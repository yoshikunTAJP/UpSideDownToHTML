// created by Й yoshikunTA
// UpSideDown To HTML

// Set up
class UpSideDown {
    // Convert to HTML
    static toHTML(text) {
        // bold
        let convertedToHTML = text.replace(/''/g, "<b>").replace(/='/g, "</b>");
        // italic
        convertedToHTML = convertedToHTML.replace(/--/g, "<i>").replace(/=-/g, "</i>");
        // strikethough
        convertedToHTML = convertedToHTML.replace(/~~/g, "<s>").replace(/=~/g, "</s>");
        // underline
        convertedToHTML = convertedToHTML.replace(/__/g, "<u>").replace(/=_/g, "</u>");
        // code block
        convertedToHTML = convertedToHTML.replace(/```/g, '<div class="code_block"><div class="code_block_text"><pre>').replace(/=`/g, '</pre></div></div>');
        // quote
        convertedToHTML = convertedToHTML.replace(/::/g, '<blockquote>').replace(/=:/g, '</blockquote>');
        // ruby
        convertedToHTML = convertedToHTML.replace(/\^ru\^(.*?) \/e= (.*?)\^ru=/g, "<ruby><rb>$1</rb><rt>$2</rt></ruby>");
        // return result
        return convertedToHTML;
    }
}